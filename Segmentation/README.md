# README #
### Segmentation Tasks ###
This directory contains all projects relating to semantic segmentation. The main directories and how to run each 
application is described below.
#### data ####
* Contains the full size and tiled images for each project. 
* Full size images are broken down into tiled images in the preprocessing code of each project. 
This enables them to be processed by the neural network.
* Tiled images and labels are split into train, val and test sets.

### roof_slope_seg ###
The data for this project was already in the correct formats for both satellite imagery and masks.
##### Instructions to Run. ######
1. Run **split_images_for_network:** to split the full sized images and masks into smaller tiles of a given size and 
move them into respective train, validation and test directories based on the ratio given in train_val_split argument.
2. Run train file to train the network and save the model to out/models. Saved example images from each 
epoch are saved to out/tmp.
3. Run inference file to run a saved model on the test data. Results are saved to out/test.

       
### general ###
* This library can be shared amongst all future segmentation projects
* All code relating to the training of the network is in here
    * **criterions.py:** Implementations of any non-standard loss functions 
    * **data.py:** The custom dataset, which loads the tiled images from a secified data folder
    * **models.py:** Contains all the segmentation models 
    * **random_trans.py:** Code for the different augmentation transformations that can be applied to the data to improve
    generalisation performance.
    * **tile_image.py:** Takes the paths of larger imagery and masks and runs a sliding window over the image to split
    it into a collection of smaller tiles, which can be processed by the nerual network more easily.
    * **train.py:** The generalised training program, which uses the arguments passed via the argparser to run a given segmentation 
    model on the data and save the model.
    * **viz.py:** General visualisation functions that can be used by each of the projects to display training progress and
    test results.
