import sys
sys.path.insert(0, "../../../")

import argparse
import os
import time
from pathlib import Path

from PIL.ImageFile import ImageFile

from Segmentation.general.criterions import iou_coef

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import torch
import numpy as np

from Segmentation.general.data import open_image, DataLoader, Dataset
from Segmentation.general.models import u_net
# import Segmentation.general.random_trans as transforms_r
# from torchvision.transforms import functional as transforms_f
from torch.autograd import Variable
from Segmentation.general.viz import plot_train_preds, save_train_preds, save_test_preds_with_labels, \
    save_test_preds_without_labels, save_test_outputs_without_labels


# def plot_train_preds_test(inputs, preds, denormalize=True):
#     import matplotlib.pyplot as plt
#
#     fig, axs = plt.subplots(inputs.size(0), 2, squeeze=False)
#     for r, imgs in enumerate(zip(inputs, preds)):
#         imgs = [im.data for im in imgs]
#         if denormalize:
#             imgs[0] = tensor_denormalize(imgs[0], Dataset.img_mean, Dataset.img_std)
#         imgs = [im.numpy().transpose(1, 2, 0) for im in imgs]
#         imgs = [im[:, :, 0] if im.shape[2] == 1 else im for im in imgs]
#         for c, img in enumerate(imgs):
#             axs[r][c].imshow(img, vmin=0, vmax=1)
#             axs[r][c].set_axis_off()
#     for i, title in enumerate(["Input", "Pred"]):
#         axs[0][i].set_title(title)
#     fig.subplots_adjust(left=0, right=1, top=1, bottom=0, wspace=0, hspace=0)
#     plt.show()


def tensor_denormalize(tensor, mean, std):
    return (tensor * torch.FloatTensor(std).unsqueeze(1).unsqueeze(2)
            + torch.FloatTensor(mean).unsqueeze(1).unsqueeze(2)) \
        .clamp(0, 1)


def main():
    parser = argparse.ArgumentParser(description='Deep roof segmentation inference')
    parser.add_argument('--data-dir', default='../../data/roof_slope/', help="Default: 'local'")
    parser.add_argument('--out-dir', default='../out/', help="Default: 'data'")
    parser.add_argument('--model-file', default='brussels_12.5cm_512_tfl.pkl', help="Default: 'footprints_v2.pkl'")
    parser.add_argument('--tile-size', default=512, help="Default: '256'")
    parser.add_argument('--rgb', action="store_true", default=False, help="Default: 'True'")
    parser.add_argument('--no-labels', action="store_true", default=False, help="Default: 'True'")
    parser.add_argument('-bs', '--batch-size', default=1, type=int, help="Default: 1")


    args = parser.parse_args()
    out_dir = Path(args.out_dir)
    test_dir = out_dir / "test"
    models_dir = out_dir / "models"
    model_file = args.model_file
    tile_size = args.tile_size
    rgb = args.rgb
    no_labels = args.no_labels

    test_accs = []

    if no_labels:
        data_dir = Path(args.data_dir)
        data_dir = data_dir / "test_images"
    else:
        data_dir = args.data_dir

    print("RBG = " + str(rgb))
    print("No labels = " + str(no_labels))
    cuda = torch.cuda.is_available()
    device = "cuda" if cuda else "cpu"
    state_dict = torch.load(models_dir / model_file, map_location=torch.device(device))

    if rgb:
        model, params = u_net(3, enc_fixed=True, no_out_act=True, cuda=cuda)
    else:
        model, params = u_net(1, enc_fixed=True, no_out_act=True, cuda=cuda)

    print("Loading data...")

    test_loader = DataLoader(
        dataset=Dataset(data_dir, tile_size, mode="test", rgb=rgb, random_trans=False, normalize=True,
                        no_labels=no_labels),
        batch_size=args.batch_size, num_workers=1, pin_memory=cuda)

    dataloaders = {"test": test_loader}
    model.load_state_dict(state_dict)


    # image= test_loader.images[0]
    normalize = False
    random_trans = False
    shuffle = False
    limit = None
    resize = None
    """if resize:
        image = transforms_f.resize(image, resize)
    if random_trans:
        image, label = random_trans([image, label])
    image = transforms_f.to_tensor(image)
    if normalize:
        image = transforms_f.normalize(image, img_mean, img_std)
"""
    # image=image.unsqueeze(0)
    print("Predicting test labels...")
    t_start = time.time()
    for i, data in enumerate(dataloaders["test"]):
        if no_labels:
            inputs = data
            labels = None
        else:
            inputs = data[0]
            labels = data[1]

        if cuda:
            inputs = inputs.cuda()
            labels = labels.cuda() if not no_labels else labels

        if inputs.dim() < 4:
            inputs = inputs.unsqueeze(0)

        logits = model(inputs)
        outputs = torch.sigmoid(logits)
        preds = torch.round(outputs)

        # add bias to green positives
        # preds[:, 1:2, :, :] = torch.where(outputs[:, 1:2, :, :] > 0.45, torch.tensor(1), torch.tensor(0))
        # add bias to red negatives
        # preds[:, 0:1, :, :] = torch.where(outputs[:, 1:2, :, :] < 0.5, torch.tensor(0), torch.tensor(1))

        if not no_labels:
            # calculate accuracy
            acc = iou_coef(labels, preds)
            test_accs.append(acc.data.item())
            print(f"Model test acc: {acc:5.2f}%")

        if no_labels:
            images = inputs, outputs, preds
        else:
            images = inputs, outputs, preds, labels

        model_name = model_file.split(".")[0]

        if no_labels:
            # save_test_preds_without_labels(test_dir, f"{model_name}_test_uk_{i}", rgb, *images[:3])
            save_test_outputs_without_labels(test_dir, f"{model_name}_test_uk_{i}", rgb, outputs)
        else:
            save_test_preds_with_labels(test_dir, f"{model_name}_test_{i}", rgb, *images)

    t_dur = time.time() - t_start
    test_acc = np.mean(test_accs)
    print("tests complete")
    print(f"Testing complete in {t_dur // 60:.0f}m {t_dur % 60:.0f}s")
    if not no_labels:
        print(f"Model test acc: {test_acc:5.2f}%")


if __name__ == "__main__":
    main()
