from Segmentation.general.tile_image import split_image_to_tiles_for_training, split_image_to_tiles_for_testing

window_size = 512
winW, winH = 512, 512

full_size_img_path = r"../../../data/roof_slope/test_images"
filetype = ".jpg"
tiled_image_path = r"../../../data/roof_slope/test_images/images/test"
resize_size = (512, 512)


split_image_to_tiles_for_testing(full_size_img_path, filetype, tiled_image_path,
                                  window_size, resize_size)
