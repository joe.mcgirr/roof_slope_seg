import argparse
import sys
from pathlib import Path
sys.path.append("../../../../")
print(sys.path)
from Segmentation.general.tile_image import split_image_to_tiles_for_training

parser = argparse.ArgumentParser(description='Split Images: Train')
parser.add_argument('--data-dir', default='../../../data/', help="Default: 'data'")

args = parser.parse_args()
data_dir = Path(args.data_dir)

window_size = 512

full_size_img_path = data_dir / "roof_slope/images(full_size)"
full_size_label_path = data_dir / "roof_slope/labels(full_size)"
tiled_image_path = data_dir / "roof_slope/images"
tiled_label_path = data_dir / "roof_slope/labels"

resize_size = (256, 256)

split_image_to_tiles_for_training(full_size_img_path, full_size_label_path, tiled_image_path, tiled_label_path,
                                  window_size, resize_size, [0.6, 0.3, 0.1])

