import argparse
import sys
import os
sys.path.append("C:/Users/joemo/my_computer_documents/roof_slope_seg")
sys.path.append("../../../")
# print(sys.path)
from Segmentation.general.train import train


def main():
    parser = argparse.ArgumentParser(description='Deep roof segmentation')
    parser.add_argument('--data-dir', default='../../data/roof_slope/', help="Default: 'data'")
    parser.add_argument('--out-dir', default='../out', help="Default: 'out'")
    parser.add_argument('--fixed', action="store_true")
    parser.add_argument('--workers', default=0, type=int, help="Default: 1")
    parser.add_argument('-e', '--epochs', default=150, type=int, help="Default: 200")
    parser.add_argument('-bs', '--batch-size', default=5, type=int, help="Default: 5")
    parser.add_argument('-lr', '--learning-rate', default=0.01, type=float, help="Default: 0.01")
    parser.add_argument('-pat', '--lr-patience', default=10, type=int,
                        help="How many epochs to wait until reducing LR when validation loss plateauing. "
                             "No LR schedule when set to 0. Default: 3")
    parser.add_argument('-fac', '--lr-factor', default=0.1, type=float,
                        help="Factor by which the LR is reduced by the scheduler. Default: 0.1")
    parser.add_argument('-m', '--momentum', default=0.9, type=float, help="Default: 0.9")
    parser.add_argument('-wd', '--weight-decay', default=0, type=float, help="Default: 0")
    parser.add_argument('-s', '--seed', default=None, type=int, help="Default: None")
    parser.add_argument('--no-gpu', action='store_true')
    parser.add_argument('--plot', action='store_true')
    parser.add_argument('--no-normalize', action='store_true')
    parser.add_argument('--no-random-trans', action='store_true')
    parser.add_argument('--rgb', action='store_false', help="Greyscale if false, RGB if true")
    parser.add_argument('--overfit', action='store_true',
                        help="Uses only one sample from the training data and no validation.")
    parser.add_argument('--limit', type=int, default=None,
                        help="Limit the number of smaples to be used for training in each epoch. Default: None")
    parser.add_argument('--tile-size', type=int, default=256,
                        help="The tile size of the network inputs. Default: 512")
    parser.add_argument('--model-name', default='new_model', help="Default: 'new_model'")
    parser.add_argument('--tl-model', default='zurich_10cm_512_lg', help="Default: 'zurich_10cm_512_lg'")
    parser.add_argument('-g', '--gamma', default=2, type=float, help="Gamma value for focal loss criterion - Default: 2")
    parser.add_argument('-a', '--alpha', default=5, type=float, help="Alhpa value for focal loss criterion - Default: 0.25")
    train(parser)


if __name__ == '__main__':
    main()