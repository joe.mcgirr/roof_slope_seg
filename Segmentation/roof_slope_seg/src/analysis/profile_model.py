import sys
import time
import torch
from torch.autograd import profiler
sys.path.append("C:/Users/joemo/my_computer_documents/roof_slope_seg")
sys.path.append(".")
sys.path.append("../../../..")
print(sys.path)
from Segmentation.general.models import u_net

num_batches = 1
cuda = torch.cuda.is_available()
print(f"running on gpu: {cuda}")
model, params = u_net(3, enc_fixed=True, no_out_act=True, cuda=cuda)
inputs = torch.randn(5, 3, 256, 256)
if cuda:
    inputs = inputs.cuda()
print("running profiler")
with profiler.profile(record_shapes=True) as prof:
    with profiler.record_function("model_inference"):
        for _ in range(0, num_batches):
            model_start = time.time()
            logits = model(inputs)
            model_dur = time.time() - model_start
            print(f"Model ran in {model_dur * 1000}ms")

# print(prof.key_averages().table(sort_by="self_cpu_memory_usage", row_limit=10))
# print(prof.key_averages().table(sort_by="cpu_memory_usage", row_limit=10))

# print(prof.key_averages().table(sort_by="cpu_time_total", row_limit=10))
# print(prof.key_averages(group_by_input_shape=True).table(sort_by="cpu_time_total", row_limit=10))