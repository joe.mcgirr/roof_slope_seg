import sys
sys.path.insert(0, "../../../../")

import argparse
import math

import torch
from torch import optim
from torch.autograd import Variable
from torch.utils.data import DataLoader

from Segmentation.general import criterions
from Segmentation.general.data import Dataset
from Segmentation.general.models import u_net
import matplotlib.pyplot as plt


def find_lr(optimizer, trn_loader, model, criterion, init_value=1e-8, final_value=20., beta=0.98, cuda=False):
    num = len(trn_loader)-1
    mult = (final_value / init_value) ** (1/num)
    lr = init_value
    optimizer.param_groups[0]['lr'] = lr
    avg_loss = 0.
    best_loss = 0.
    batch_num = 0
    losses = []
    log_lrs = []
    for data in trn_loader:
        batch_num += 1
        #As before, get the loss for this mini-batch of inputs/outputs
        inputs,labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        if cuda:
            inputs, labels = inputs.cuda(), labels.cuda()

        optimizer.zero_grad()
        logits = model(inputs)
        outputs = torch.sigmoid(logits)
        loss = criterion(outputs, labels)
        #Compute the smoothed loss
        avg_loss = beta * avg_loss + (1-beta) * loss.data
        smoothed_loss = avg_loss / (1 - beta**batch_num)
        #Stop if the loss is exploding
        if batch_num > 1 and smoothed_loss > 4 * best_loss:
            return log_lrs, losses
        #Record the best loss
        if smoothed_loss < best_loss or batch_num==1:
            best_loss = smoothed_loss
        #Store the values
        losses.append(smoothed_loss.data.cpu())
        log_lrs.append(math.log10(lr))
        #Do the SGD step
        loss.backward()
        optimizer.step()
        #Update the lr for the next step
        lr *= mult
        print(f"new lr: {lr}")
        optimizer.param_groups[0]['lr'] = lr
    return log_lrs, losses

def main():
    parser = argparse.ArgumentParser(description='Deep roof segmentation')
    parser.add_argument('--data-dir', default='../../data/roof_slope/', help="Default: 'data'")
    parser.add_argument('--out-dir', default='../out', help="Default: 'out'")
    parser.add_argument('--tile-size', type=int, default=512,
                        help="The tile size of the network inputs. Default: 512")
    parser.add_argument('-bs', '--batch-size', default=5, type=int, help="Default: 5")
    parser.add_argument('--init-value', default=1e-8, type=int, help="Default: 5")
    parser.add_argument('--final-value', default=10, type=int, help="Default: 5")

    args = parser.parse_args()

    gamma = 2
    alpha = 5
    cuda = torch.cuda.is_available()
    model, params = u_net(3, enc_fixed=False, no_out_act=True, cuda=cuda)
    optimizer = optim.SGD(params, lr=1e-1, momentum=0.9, weight_decay=0)
    criterion = criterions.FocalLoss(logits=True, gamma=gamma, alpha=alpha)

    train_loader = DataLoader(
        dataset=Dataset(args.data_dir, mode="train", rgb=True, tile_size=args.tile_size, random_trans=True,
                        normalize=True, limit=None),
        batch_size=args.batch_size, shuffle=True, num_workers=4, pin_memory=cuda)

    logs, losses = find_lr(optimizer, train_loader, model, criterion, args.init_value, args.final_value,  cuda=cuda)

    plt.plot(logs[10:-5], losses[10:-5])
    plt.xlabel('Learning rate (log scale)')
    plt.ylabel('Loss')
    plt.savefig('lr_losses_simple2.png')


if __name__ == '__main__':
    main()