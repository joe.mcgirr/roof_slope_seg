#!/usr/bin/bash
source /usr2/share/gpu.sbatch
srun python roof_slope_train.py --data-dir /db/psyjm6/roof_slope_lg/ --out-dir /db/psyjm6/out --tile-size 512 --rgb -bs 5 --model-name zurich_10cm_512_lg_e_20 --workers 8 -e 20
