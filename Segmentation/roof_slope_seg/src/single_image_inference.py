import argparse
import time
from pathlib import Path

import torch
from PIL import Image

from torchvision.transforms import functional as transforms_f
from Segmentation.general.models import u_net


def open_image(path, mode=None):
    i = Image.open(path, 'r')
    i.load()
    if mode and i.mode != mode:
        i = i.convert(mode)
    return i


def main():
    parser = argparse.ArgumentParser(description='Deep roof segmentation inference')
    parser.add_argument('--data-dir', default='../../data/roof_slope/inference', help="Default: 'local'")
    parser.add_argument('--out-dir', default='../out/', help="Default: 'data'")
    parser.add_argument('--input-file', default='brussels_img3.png', help="Default: 'data'")
    parser.add_argument('--model-file', default='zurich_10cm_512_lg.pkl', help="Default: 'footprints_v2.pkl'")
    parser.add_argument('--tile-size', default=512, help="Default: '256'")
    parser.add_argument('--rgb', action="store_true", default=True, help="Default: 'True'")

    args = parser.parse_args()
    out_dir = Path(args.out_dir)
    data_dir = Path(args.data_dir)
    test_dir = out_dir / "test"
    models_dir = out_dir / "models"
    model_file = args.model_file
    tile_size = args.tile_size
    rgb = args.rgb

    print("Loading data...")
    cuda = torch.cuda.is_available()
    device = "cuda" if cuda else "cpu"
    state_dict = torch.load(models_dir / model_file, map_location=torch.device(device))

    if rgb:
        model, params = u_net(3, enc_fixed=True, no_out_act=True, cuda=cuda)
    else:
        model, params = u_net(1, enc_fixed=True, no_out_act=True, cuda=cuda)

    model.load_state_dict(state_dict)

    t_start = time.time()

    input = open_image(data_dir / args.input_file, "RGB")
    input = transforms_f.to_tensor(input)
    if cuda:
        input = input.cuda()

    if input.dim() < 4:
        input = input.unsqueeze(0)

    output = torch.round(torch.sigmoid(model(input)))
    images = input, output
    model_name = model_file.split(".")[0]

    im = transforms_f.to_pil_image(output[0].cpu(), "RGB")
    path = Path(f"{data_dir}/masks/{args.input_file}")
    im.save(path)

    t_dur = time.time() - t_start
    print("tests complete")
    print(f"Inference complete in {t_dur // 60:.0f}m {t_dur % 60:.0f}s")



if __name__ == "__main__":
    main()