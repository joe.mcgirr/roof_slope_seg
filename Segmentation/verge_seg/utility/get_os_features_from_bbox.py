import folium as folium
import requests
# from shapely.geometry import Polygon
from PIL.Image import Image
from Segmentation.verge_seg.utility.osm_geotiff import osm_geotiff
import numpy as np
import gdal
import PIL
import cv2

# OS Data Hub WFS endpoint path
wfs_endpoint = 'https://api.os.uk/features/v1/wfs?'
WGS84 = 'epsg:4326'
lon_width, lat_height = [0.0038, 0.004]
bbox_centre_lat_lon = [52.299164, -1.588424]
zoom = 18


def create_bbox(lat, lon, lat_height, lon_width):
    # can be set either in EPSG:27700 (BNG) or EPSG:3857 (Web Mercator)
    # might need to change this to convert from WGS 84 Web Mercator (google)
    lon_min = lon - lon_width
    lon_max = lon + lon_width
    lat_min = lat - lat_height
    lat_max = lat + lat_height
    # bottom-left x, bottom-left y, top-right x, top-right y
    return lat_min, lon_min, lat_max, lon_max,


def create_request_filter(bbox, descr_group):
    # Create an OGC XML filter parameter value which will only select the feature defined in descr_group
    filter_xml = '<ogc:Filter>'
    filter_xml += '<ogc:And>'
    filter_xml += '<ogc:Overlaps>'
    filter_xml += '<PropertyName>SHAPE</PropertyName>'
    filter_xml += '<gml:Box xmlns:gml="http://www.opengis.net/gml" srsName="urn:ogc:def:crs:EPSG::4326">'
    filter_xml += '<gml:coordinates decimal="." cs="," ts=",">' + ','.join(str(s) for s in bbox) + '</gml:coordinates>'
    filter_xml += '</gml:Box>'
    filter_xml += '</ogc:Overlaps>'
    filter_xml += '<ogc:PropertyIsEqualTo>'
    filter_xml += '<ogc:PropertyName>DescriptiveGroup</ogc:PropertyName>'
    filter_xml += '<ogc:Literal>' + descr_group + '</ogc:Literal>'
    filter_xml += '</ogc:PropertyIsEqualTo>'
    filter_xml += '</ogc:And>'
    filter_xml += '</ogc:Filter>'
    return filter_xml


def project_to_img_coords(lon, lat, datasource):
    # east, north = convert_espg4326_to_3857(lat, lon)
    # corresponding row and column in our image
    row, col = datasource.index(lon, lat)
    return row, col


def get_mapbox_image(bbox):
    path = osm_geotiff.createGeoTIFF(bbox[1], bbox[3], bbox[0], bbox[2], zoom, True)
    return path


def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((y - ulY) / yDist)
    return (pixel, line)


def get_roadside_features(bbox):
    api_key = 'OltgIEJQ6giNv5xDMt1Fujs0PCk1uegS'
    # Define additional WFS parameters
    count = 100
    service = 'wfs'
    request = 'GetFeature'
    version = '2.0.0'
    typeNames = 'Topography_TopographicArea'
    # will default to EPSG:4326 (WGS84) coord system
    outputFormat = 'GEOJSON'
    srsName = 'EPSG:4326'

    # Create empty list to be populated with building features
    features = []

    # loop required for paging if > 100 features returned. Unlikely for bbox as small as we use.
    for i in range(0, 1):
        # Offset startIndex value by 100 for each request
        startIndex = i * 100
        filter_xml = create_request_filter(bbox, "Roadside")

        # Represent WFS parameters in a dictionary
        params_wfs = {'key': api_key,
                      'count': count,
                      'service': service,
                      'request': request,
                      'version': version,
                      'startIndex': startIndex,
                      'typeNames': typeNames,
                      'outputFormat': outputFormat,
                      'srsName': srsName,
                      'filter': filter_xml
                      }

        # Make HTTP GET request and raise exception if request was unsuccessful
        try:
            r = requests.get(wfs_endpoint, params=params_wfs)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            print(e)
        # Decode JSON payload returned by request
        payload = r.json()
        # Check if features object is empty
        # If empty all feature in BBOX have been returned
        if not payload['features']:
            break
        # If not empty append feature data to feature list
        else:
            for j in range(0, len(payload['features'])):
                features.append(payload['features'][j])

    # Represent features in GeoJSON feature collection
    geojson = {'type': 'FeatureCollection',
               'features': features}

    return features


def convert_geo_coords_to_pixels(geo_coords, geo_transform):
    im_coords = []
    for coord in geo_coords:
        pix_x, pix_y = world2Pixel(geo_transform, *coord)
        np_coords = np.expand_dims(np.array([pix_y, pix_x]), 0)
        im_coords.append(np_coords)
    return np.array(im_coords)


def main():
    # Define a OGC WFS filter compliant bounding box for the polygon geometry (https://www.ogc.org/standards/wfs)
    # bottom-left y, bottom-left x, top-right y, top-right x
    bbox = create_bbox(*bbox_centre_lat_lon, lon_width, lat_height)
    # get satellite image .TIF file for given centre point, width and height
    # sat_img_path = get_mapbox_image(bbox)
    # satellite_img = gdal.Open(sat_img_path)
    satellite_img = gdal.Open(r"..\mosaic_-1.592424_52.295364_18_satellite.tif")
    # get the 'Roadside' features at the same centre point, width and height
    road_side_features = get_roadside_features(bbox)
    # project 'Roadside' features onto the satellite TIF
    # Obtain lon/lat coords from geoTIFF
    gt = satellite_img.GetGeoTransform()
    # generate empty mask to start
    mask = np.zeros((satellite_img.RasterXSize, satellite_img.RasterYSize), dtype=np.uint8)
    # iterate through each returned feature and draw its polygon on the mask
    m = folium.Map(location=bbox_centre_lat_lon, tiles='https://api.mapbox.com/styles/v1/unisoft/ckh0hfxd30fiv19phiqt53t1a/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidW5pc29mdCIsImEiOiJja2RoajdwN3AzMTY3MnJzODd2cGRpdnBlIn0.KCJ1NNMcaYzqbJmMZ-ehmw',
                   attr='_satellite', zoom_start=19)
    for idx, feature in enumerate(road_side_features):
        feature_coords = feature['geometry']['coordinates'][0]
        image_coords = convert_geo_coords_to_pixels(feature_coords, gt)
        mask = cv2.drawContours(mask, [image_coords], 0, color=255, thickness=-1)

        folium.GeoJson(
            feature
        ).add_to(m)

    m.save("./mymap.html")
    cv2.imwrite(r"../masks/test_mask.png", mask.transpose(1, 0))

    # mask = mask.astype(np.bool)

    # save satellite image
    # save mask image


if __name__ == '__main__':
    main()
