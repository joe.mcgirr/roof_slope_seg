# import required libraries
import glob

import rasterio
from rasterio import plot
import matplotlib.pyplot as plt
import numpy as np
import os
from contextlib import contextmanager

from rasterio import Affine, MemoryFile
from rasterio.enums import Resampling
from rasterio.warp import reproject, calculate_default_transform
from rasterio.windows import Window, from_bounds

lon_width, lat_height = [0.0038, 0.004]
bbox_centre_lat_lon = [52.299164, -1.588424]

sentinel_dir = r"google_maps/sentinel_data" \
               r"/S2B_MSIL2A_20191223T111359_N0213_R137_T30UWC_20191223T123500.SAFE/GRANULE" \
               r"/L2A_T30UWC_A014606_20191223T111437/IMG_DATA/R10m/"


def create_bbox(lat, lon, lat_height, lon_width):
    lon_min = lon - lon_width
    lon_max = lon + lon_width
    lat_min = lat - lat_height
    lat_max = lat + lat_height
    return lon_min, lat_min, lon_max, lat_max


def reproject_raster(in_path, out_path, crs):
    # reproject raster to project crs
    with rasterio.open(in_path) as src:
        src_crs = src.crs
        crs = {'init': crs}
        transform, width, height = calculate_default_transform(src_crs, crs, src.width, src.height, *src.bounds)
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': crs,
            'transform': transform,
            'width': width, 
            'height': height})

        with rasterio.open(out_path, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=crs,
                    resampling=Resampling.nearest)
    return (out_path)


def crop_bbox_region(image, lon_min, lat_min, lon_max, lat_max):
    window = from_bounds(lon_min, lat_min, lon_max, lat_max, image.transform)
    # image_cropped = image.read(1,
    #                            window=window,
    #                            out_shape=(
    #                                image.count,
    #                                5120,
    #                                3584
    #                            ),
    #                            resampling=Resampling.bilinear)

    # generate nir and red objects as arrays in float64 format
    image_cropped = image.read(1, window=window).astype('float64')

    # scale image transform
    # transform = image.transform * image.transform.scale(
    #     (image.width / image_cropped.shape[-1]),
    #     (image.height / image_cropped.shape[-2])
    # )
    return image_cropped, window


# use context manager so DatasetReader and MemoryFile get cleaned up automatically
@contextmanager
def resample_raster(raster, scale=2):
    t = raster.transform

    # rescale the metadata
    transform = Affine(t.a / scale, t.b, t.c, t.d, t.e / scale, t.f)
    height = raster.height * scale
    width = raster.width * scale

    profile = raster.profile
    profile.update(transform=transform, driver='GTiff', height=height, width=width)

    data = raster.read( # Note changed order of indexes, arrays are band, row, col order not row, col, band
            out_shape=(raster.count, height, width),
            resampling=Resampling.bilinear,
        )

    with rasterio.open(
            ndvi_path,
            'w',
            **profile
    ) as ndvi_raster:
        ndvi_raster.write(data.squeeze(), 1)

    with MemoryFile() as memfile:
        with memfile.open(**profile) as dataset: # Open as DatasetWriter
            dataset.write(data)
            del data

        with memfile.open() as dataset:  # Reopen as DatasetReader
            yield dataset  # Note yield not return


def main():
    lon_min, lat_min, lon_max, lat_max = create_bbox(*bbox_centre_lat_lon, lon_width, lat_height)

    # Search directory for desired bands
    red_file = glob.glob(sentinel_dir + '**B04_10m.jp2')  # red band
    nir_file = glob.glob(sentinel_dir + '**B08_10m.jp2')  # nir band

    # red_file = reproject_raster(red_file[0],  (red_file[0].split(".jp2")[0] + ".tif"), 'EPSG:4326')
    # nir_file = reproject_raster(nir_file[0],  (nir_file[0].split(".jp2")[0] + ".tif"), 'EPSG:4326')
    red_file = r"google_maps\sentinel_data\S2B_MSIL2A_20191223T111359_N0213_R137_T30UWC_20191223T123500.SAFE\GRANULE\L2A_T30UWC_A014606_20191223T111437\IMG_DATA\R10m\T30UWC_20191223T111359_B04_10m.tif"
    nir_file = r"google_maps\sentinel_data\S2B_MSIL2A_20191223T111359_N0213_R137_T30UWC_20191223T123500.SAFE\GRANULE\L2A_T30UWC_A014606_20191223T111437\IMG_DATA\R10m\T30UWC_20191223T111359_B08_10m.tif"

    band4 = rasterio.open(red_file)  # red
    band8 = rasterio.open(nir_file)  # nir

    red, window = crop_bbox_region(band4, lon_min, lat_min, lon_max, lat_max)
    nir, window = crop_bbox_region(band8, lon_min, lat_min, lon_max, lat_max)

    # ndvi calculation, empty cells or nodata cells are reported as 0
    ndvi = np.where((nir + red) == 0., 0, (nir - red) / (nir + red))

    ndvi_path = red_file.split("B04_10m")[0] + "NDVI.tif"
    # export ndvi image
    with rasterio.open(
            ndvi_path,
            'w',
            driver='GTiff',
            height=red.shape[0],
            width=red.shape[1],
            count=1,
            dtype=red.dtype,
            crs=band4.crs,
            transform=band4.window_transform(window)
    ) as ndvi_raster:
        ndvi_raster.write(ndvi, 1)

    with rasterio.open(ndvi_path) as src:
        with resample_raster(src, 100) as resampled:
            print('Orig dims: {}, New dims: {}'.format(src.shape, resampled.shape))
            print(repr(resampled))

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
    # plot ndvi
    ndvi = rasterio.open(red_file.split("B04_10m")[0] + "NDVI.tif")
    fig = plt.figure(figsize=(18, 12))
    plot.show(ndvi)

    print("")


if __name__ == '__main__':
    main()