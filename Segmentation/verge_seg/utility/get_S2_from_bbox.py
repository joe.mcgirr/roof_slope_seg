import numpy as np
from sentinelsat.sentinel import SentinelAPI, read_geojson, geojson_to_wkt
import matplotlib.pyplot as plt
from shapely.geometry import Polygon, Point
# connect to the API
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date
import zipfile

user = "joemo58"
password = "UNIsoftC01&"
lon_width, lat_height = [0.0038, 0.004]
bbox_centre_lat_lon = [52.299164, -1.588424]
sentinel_dir = "google_map/sentinel_data/"

def create_bbox(lat, lon, lat_height, lon_width):
    # can be set either in EPSG:27700 (BNG) or EPSG:3857 (Web Mercator)
    # might need to change this to convert from WGS 84 Web Mercator (google)
    lon_min = round(lon - lon_width, 4)
    lon_max = round(lon + lon_width, 4)
    lat_min = round(lat - lat_height, 4)
    lat_max = round(lat + lat_height, 4)
    # bottom-left x, bottom-left y, top-right x, top-right y
    point_list = [Point(lon_min, lat_min), Point(lon_min, lat_max), Point(lon_max, lat_max), Point(lon_min, lat_max),
                  Point(lon_min, lat_min)]
    poly = Polygon([[p.x, p.y] for p in point_list])
    # convert to well-known text representation for sentinel API
    wkt_poly = poly.wkt
    return wkt_poly



def main():
    api = SentinelAPI(user, password, 'https://scihub.copernicus.eu/dhus')

    # search by polygon, time, and SciHub query keywords
    bbox_polygon = create_bbox(*bbox_centre_lat_lon, lon_width, lat_height)
    #footprint_2 = geojson_to_wkt(read_geojson('google_maps/bbox.geojson'))
    products = api.query(bbox_polygon,
                         date=('20191219', date(2019, 12, 29)),
                         platformname='Sentinel-2',
                         area_relation="Contains",
                         limit=0,
                         processinglevel='Level-2A',
                         cloudcoverpercentage=(0, 30))

    # download all results from the search
    success = api.download_all(products)

    # fix this part.
    zip_ref = zipfile.ZipFile("..", 'r')
    zip_ref.extractall(sentinel_dir)
    zip_ref.close()


if __name__ == '__main__':
    main()