# Takes a bounding box, and downloads tiles from mapbox within this box
# Stitches tiles together and creates geoTIFF from them

from math import log, tan, radians, cos, pi, floor, degrees, atan, sinh
import numpy as np
import glob
import subprocess
import urllib.request
import os
import gdal
import Scripts.gdal_merge as gm

#tile_server = "https://a.tile.openstreetmap.org/${z}/${x}/${y}.png"


class osm_geotiff():
    def __init__(self, lat, lon):
            self.lat = lat
            self.lon = lon

    def latlon_to_xyz(lat, lon, z):
        tile_count = pow(2, z)
        x = (lon + 180) / 360
        y = (1 - log(tan(radians(lat)) + (1/cos(radians(lat)))) / pi) / 2
        return(tile_count*x, tile_count*y)

    def bbox_to_xyz(lon_min, lon_max, lat_min, lat_max, z):
        x_min, y_max = osm_geotiff.latlon_to_xyz(lat_min, lon_min, z)
        x_max, y_min = osm_geotiff.latlon_to_xyz(lat_max, lon_max, z)
        return(floor(x_min), floor(x_max),
            floor(y_min), floor(y_max))

    def download_tile(x, y, z, tile_server):
        url = tile_server.replace(
            "{x}", str(x)).replace(
            "{y}", str(y)).replace(
            "{z}", str(z))
        print(url)
        path = f'google_maps/geoTIFF/osm_{x}_{y}_{z}.png'
        urllib.request.urlretrieve(url, path)
        return(path)

    def x_to_lon_edges(x, z):
        tile_count = pow(2, z)
        unit = 360 / tile_count
        lon1 = -180 + x * unit
        lon2 = lon1 + unit
        return(lon1, lon2)

    def mercatorToLat(mercatorY):
        return(degrees(atan(sinh(mercatorY))))

    def y_to_lat_edges(y, z):
        tile_count = pow(2, z)
        unit = 1 / tile_count
        relative_y1 = y * unit
        relative_y2 = relative_y1 + unit
        lat1 = osm_geotiff.mercatorToLat(pi * (1 - 2 * relative_y1))
        lat2 = osm_geotiff.mercatorToLat(pi * (1 - 2 * relative_y2))
        return(lat1, lat2)

    def tile_edges(x, y, z):
        lat1, lat2 = osm_geotiff.y_to_lat_edges(y, z)
        lon1, lon2 = osm_geotiff.x_to_lon_edges(x, z)
        return[lon1, lat1, lon2, lat2]

    def georeference_raster_tile(x, y, z, path):
        bounds = osm_geotiff.tile_edges(x, y, z)
        filename, extension = os.path.splitext(path)
        gdal.Translate(filename + '.tif',
                    path,
                    outputSRS='EPSG:4326',
                    outputBounds=bounds)


    def createGeoTIFF(lon_min, lon_max: object, lat_min: object, lat_max: object, zoom: object, satellite: object) -> object:

        # Get satellite or building outline depending on if 'satellite' is True or False
        if satellite==True:
            tile_server = "https://api.mapbox.com/styles/v1/unisoft/ckh0hfxd30fiv19phiqt53t1a/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidW5pc29mdCIsImEiOiJja2RoajdwN3AzMTY3MnJzODd2cGRpdnBlIn0.KCJ1NNMcaYzqbJmMZ-ehmw"
            plus = "_satellite"
        else:
            tile_server = "https://api.mapbox.com/styles/v1/unisoft/ckdtzniuq09h51apks2a01r2k/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidW5pc29mdCIsImEiOiJja2RoajdwN3AzMTY3MnJzODd2cGRpdnBlIn0.KCJ1NNMcaYzqbJmMZ-ehmw"
            plus = ""
        # Can change/create a style on mapbox.com to use instead
        
        x_min, x_max, y_min, y_max = osm_geotiff.bbox_to_xyz(
            lon_min, lon_max, lat_min, lat_max, zoom)

        for x in range(x_min, x_max + 1):
            for y in range(y_min, y_max + 1):
                #print(f"{x},{y}")
                png_path = osm_geotiff.download_tile(x, y, zoom, tile_server)
                osm_geotiff.georeference_raster_tile(x, y, zoom, png_path)

        files_to_mosaic = glob.glob("google_maps/geoTIFF/osm_*.tif")
        files_string = " ".join(files_to_mosaic)
        print(files_string)
        img_path = "google_maps/geoTIFF/mosaic_"+str(lon_min)+"_"+str(lat_min)+"_"+str(zoom)+plus+".tif"
        gm.main(['', '-o', "google_maps/geoTIFF/mosaic_"+str(lon_min)+"_"+str(lat_min)+"_"+str(zoom)+plus+".tif", *files_to_mosaic])
        for name in glob.glob("google_maps/geoTIFF/osm_*.tif"):
            os.remove(name)
        for name in glob.glob("google_maps/geoTIFF/osm_*.png"):
            os.remove(name)
            
        return img_path

