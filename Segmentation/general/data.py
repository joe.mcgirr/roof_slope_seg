import time
from pathlib import Path

import numpy as np
from PIL import Image
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torchvision.transforms import functional as transforms_f

import Segmentation.general.random_trans as transforms_r

from PIL.ImageFile import ImageFile
# enable truncated files -
ImageFile.LOAD_TRUNCATED_IMAGES = True


class Dataset(Dataset):
    img_mean = [0.44342881441116333, 0.46455633640289307, 0.4434606432914734]
    img_std = [0.16203945875167847, 0.15828818082809448, 0.16288381814956665]

    def __init__(self, data_dir, tile_size, mode="train", rgb=True, random_trans=False, normalize=True, resize=None, limit=None,
                 no_labels=False):
        self.mode = "RGB" if rgb else "L"
        self.images = self.load_image_paths(Path(f"{data_dir}/images/{mode}"))
        self.labels = self.load_image_paths(Path(f"{data_dir}/labels/{mode}")) if not no_labels else None
        if limit:
            self.images, self.labels = self.images[:limit], self.labels[:limit]
        self.normalize = normalize
        self.resize = resize
        self.random_trans = transforms_r.RandomSubset([
            transforms_r.RandomHorizontalFlipUnison(),
            transforms_r.RandomVerticalFlipUnison(),
            transforms_r.RandomResizedCropUnison(tile_size, scale=(0.5, 1.0), ratio=(3 / 4, 4 / 3)),
            transforms_r.RandomRotationUnison(180)
        ]) if random_trans else None

    @staticmethod
    def load_image_paths(dir_: Path, mode=None):
        paths = sorted([p for p in dir_.iterdir() if p.is_file()])
        return paths

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        image = open_image(self.images[idx], mode="RGB")
        label = open_image(self.labels[idx], self.mode) if self.labels else None
        if self.resize:
            image = transforms_f.resize(image, self.resize)
        if self.random_trans:
            image, label = self.random_trans([image, label])
        image = transforms_f.to_tensor(image)
        if self.normalize:
            image = transforms_f.normalize(image, self.img_mean, self.img_std)
        if label:
            label = transforms_f.to_tensor(label)
            return image, label
        else:
            return image


def open_image(path, mode=None):
    i = Image.open(path, 'r')
    i.load()
    if mode and i.mode != mode:
        i = i.convert(mode)
    return i


def get_mean_std():
    dataset = Dataset("../data", mode="train", random_trans=False, normalize=False, resize=False, limit=None)
    nb_samples = len(dataset)
    loader = DataLoader(dataset, batch_size=nb_samples)
    images, _ = next(iter(loader))
    means = np.mean(images.numpy().transpose(0, 2, 3, 1), axis=(0, 1, 2))
    stds = np.std(images.numpy().transpose(0, 2, 3, 1), axis=(0, 1, 2))
    return means, stds


if __name__ == "__main__":
    # calculate mean and std for the training data
    mean, std = get_mean_std()
    print(f"Traing data mean: {mean.tolist()}, std: {std.tolist()}")
