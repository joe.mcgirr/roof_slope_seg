import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np


class FocalLoss(nn.Module):
    def __init__(self, alpha=5, gamma=2, logits=False, reduce=True):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduce = reduce

    def forward(self, inputs, targets):
        if self.logits:
            BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction='none')
        else:
            BCE_loss = F.binary_cross_entropy(inputs, targets, reduction='none')
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

        if self.reduce:
            return torch.mean(F_loss)
        else:
            return F_loss



def tversky(labels, preds, smooth=1, alpha=0.7):
    y_label_pos = torch.flatten(labels)
    y_pred_pos = torch.flatten(preds)
    true_pos = torch.sum(y_label_pos * y_pred_pos)
    false_neg = torch.sum(y_label_pos * (1 - y_pred_pos))
    false_pos = torch.sum((1 - y_label_pos) * y_pred_pos)
    return (true_pos + smooth) / (true_pos + alpha * false_neg + (1 - alpha) * false_pos + smooth)


class FocalTverskyLoss(nn.Module):

    def __init__(self, gamma=0.75, alpha=0.7, logits=False, reduce=True, cuda=False):
        super(FocalTverskyLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduce = reduce

    def forward(self, output, targets):
        tv = tversky(targets, output, smooth=1, alpha=self.alpha)
        return (1 - tv) ** self.gamma


class SoftDiceLoss(nn.Module):

    def __init__(self, smooth=1):
        """:arg smooth The parameter added to the denominator and numerator to prevent the division by zero"""

        super().__init__()
        self.smooth = smooth

    def forward(self, input, target):
        assert torch.max(target).item() <= 1, 'SoftDiceLoss() is only available for binary classification.'

        batch_size = input.size(0)
        probability = F.softmax(input, dim=1)

        # Convert probability and target to the shape [B, (C*H*W)]
        probability = probability.view(batch_size, -1)
        target_one_hot = F.one_hot(target, num_classes=self.num_classes).permute((0, 3, 1, 2))
        target_one_hot = target_one_hot.contiguous().view(batch_size, -1)

        intersection = probability * target

        dsc = (2 * intersection.sum(dim=1) + self.smooth) / (
                target_one_hot.sum(dim=1) + probability.sum(dim=1) + self.smooth)
        loss = (1 - dsc).sum()

        return loss


def iou_score(labels, preds, smooth):
    intersection = torch.sum(torch.abs(labels * preds))
    # without the subtraction the intersection would be counted twice
    union = torch.sum(labels) + torch.sum(preds) - intersection
    iou = (intersection + smooth) / (union + smooth)
    return iou


def iou_coef(labels, preds, smooth=1):
    num_classes = preds.size(1)
    total_iou = 0
    with torch.no_grad():
        # model should produce 3 colours, red, green and blue.
        for cls in range(1, num_classes):
            iou = iou_score(labels[:, cls-1:cls, :, :], preds[:, cls-1:cls, :, :], smooth)
            total_iou += iou
    mean_iou = total_iou / num_classes
    return mean_iou


def pixel_wise_acc(labels, preds):
    nb_wrong = (labels - preds).nonzero().size(0)
    nb_total = labels.size(0) * labels.size(1) * labels.size(2) * labels.size(3)
    acc = (1 - nb_wrong / nb_total) * 100
    return acc