import argparse
import copy
import shutil
import time
from pathlib import Path
import os
import sys
from torch.utils.tensorboard import SummaryWriter
from Segmentation.general.criterions import pixel_wise_acc, iou_coef

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import numpy as np
import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader

from Segmentation.general.data import Dataset
from Segmentation.general.models import u_net, u_net_plus_plus
from Segmentation.general.viz import plot_train_preds, save_train_preds, save_test_preds
from Segmentation.general import criterions


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


def train(parser):
    print("general setup...")
    args = parser.parse_args()
    rgb = not args.rgb
    cuda = not args.no_gpu and torch.cuda.is_available()
    if args.seed:
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        if cuda:
            torch.cuda.manual_seed(args.seed)
    out_dir = Path(args.out_dir)
    model_name = args.model_name
    tmp_dir = out_dir / "tmp"
    if tmp_dir.exists():
        shutil.rmtree(tmp_dir)
    tmp_dir.mkdir()
    model_dir = out_dir / "models"
    model_dir.mkdir(exist_ok=True)
    test_dir = out_dir / "test"
    test_dir.mkdir(exist_ok=True)

    print("Preparing model...")
    if rgb:
        # model, params = u_net_plus_plus(in_channels=3, num_classes=3,  cuda=cuda, res=args.tile_size)
        model, params = u_net(3, enc_fixed=args.fixed, no_out_act=True, cuda=cuda)
    else:
        model, params = u_net(1, enc_fixed=args.fixed, no_out_act=True, cuda=cuda)

    # transfer learning - load model parameters into model
    tl_model_file = args.tl_model + ".pkl"
    state_dict = torch.load(model_dir / tl_model_file, map_location=torch.device("cuda" if cuda else "cpu"))
    model.load_state_dict(state_dict)

    # criterion = nn.BCEWithLogitsLoss()
    # alpha adds bias towards positive values and gamma adds bias to hard to classify instances
    criterion = criterions.FocalLoss(logits=True, gamma=args.gamma, alpha=args.alpha)
    optimizer = optim.SGD(params, lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
    scheduler = lr_scheduler.ReduceLROnPlateau(
        optimizer, factor=args.lr_factor, patience=args.lr_patience, verbose=True) \
        if args.lr_patience > 0 and 1 > args.lr_factor > 0 else None
    writer = SummaryWriter(f'runs/{model_name}')

    print("Loading data...")
    normalize = not args.no_normalize
    random_trans = not args.overfit and not args.no_random_trans
    shuffle = not args.overfit
    limit = args.limit if not args.overfit else 1
    batch_size = args.batch_size if not limit or args.batch_size <= limit else limit
    train_loader = DataLoader(
        dataset=Dataset(args.data_dir, mode="train", rgb=rgb, tile_size=args.tile_size, random_trans=random_trans,
                        normalize=normalize, limit=limit),
        batch_size=batch_size, shuffle=shuffle, num_workers=args.workers, pin_memory=cuda)
    val_loader = DataLoader(
        dataset=Dataset(args.data_dir, mode="val", rgb=rgb, tile_size=args.tile_size, random_trans=False,
                        normalize=normalize),
        batch_size=batch_size, num_workers=args.workers, pin_memory=cuda)
    test_loader = DataLoader(
        dataset=Dataset(args.data_dir, mode="test", rgb=rgb, tile_size=args.tile_size, random_trans=False,
                        normalize=normalize, no_labels=True),
        batch_size=batch_size, num_workers=args.workers, pin_memory=cuda)
    phases = ["train"]
    dataloaders = {"train": train_loader, "test": test_loader}
    if not args.overfit:
        phases.append("val")
        dataloaders["val"] = val_loader

    print("Training and evaluating...")
    t_start = time.time()
    it_durs = []  # iteration durations
    val_losses, val_accs = [], []
    best_acc, best_weights = -1, None
    for epoch in range(args.epochs):
        print(f"Epoch {epoch + 1}/{args.epochs}\n{'-' * 10}")
        for phase in phases:
            model.train(phase == "train")
            # loop through data batches
            for i, (inputs, labels) in enumerate(dataloaders[phase]):
                t_it_start = time.time()
                inputs, labels = Variable(inputs), Variable(labels)

                # acc = iou_coef(labels, preds)
                if cuda:
                    inputs, labels = inputs.cuda(), labels.cuda()

                # predict
                optimizer.zero_grad()
                logits = model(inputs)
                outputs = torch.sigmoid(logits)

                loss = criterion(logits, labels)

                # should round green channel to 1 if probability >= 0.3, rather than 0.5 to promote ridge predictions
                preds = torch.round(outputs)

                # eval accuracy
                acc = pixel_wise_acc(labels, preds)
                # acc = iou_coef(labels, preds)

                if phase == "train":
                    print(f"batch: {i}, {phase} loss: {loss.data.item():.4f}, acc: {acc:5.2f}%")
                elif phase == "val":
                    val_losses.append(loss.data.item())
                    val_accs.append(acc)
                # learn
                loss.backward()
                optimizer.step()

                t_it_dur = time.time() - t_it_start
                if phase == "train":
                    it_durs.append(t_it_dur)  # only count training iterations

                images = inputs, outputs, preds, labels

                # 	save preview img to tmp every 100 images processed
                if i % 400 == 0:
                    writer.add_scalar("Loss/train", loss, epoch)
                    writer.add_scalar("Accuracy/train", acc, epoch)

                    if cuda:
                        images = [i.cpu() for i in images]
                    if args.plot and phase == "train":
                        plot_train_preds(*images, denormalize=normalize)

                    save_train_preds(tmp_dir, f"{epoch}_{phase}_{i}", *images, denormalize=normalize, rgb=rgb)

            if phase == "val":
                val_loss, val_acc = np.mean(val_losses), np.mean(val_accs)
                its_per_sec = 1 / np.mean(it_durs)
                print(f"{phase} loss: {val_loss:.4f}, acc: {val_acc:5.2f}%, it/s: {its_per_sec:.2f}")
                if scheduler:
                    scheduler.step(val_loss)
                    print(f"New learning rate = {get_lr(optimizer)}")
                    writer.add_scalar("L.R", get_lr(optimizer), epoch)

                if val_acc > best_acc:
                    best_acc = val_acc
                    # best_weights = copy.deepcopy(model.state_dict())
                    print("new best acc!")
                    print("Storing model...")
                    # save model rather than saving weights to save model later. Enables early breakout.
                    torch.save(model.state_dict(), model_dir / f"{model_name}.pkl")
                val_losses, val_accs = [], []

    t_dur = time.time() - t_start
    print(f"Training complete in {t_dur // 60:.0f}m {t_dur % 60:.0f}s")
    print(f"Best model val acc: {best_acc:5.2f}%")
    # model.load_state_dict(best_weights)
    #
    # print("Storing model...")
    # torch.save(model.state_dict(), model_dir / f"{model_name}.pkl")
    writer.flush()
    writer.close()

    print("Predicting test labels...")
    for inputs in dataloaders["test"]:
        inputs = Variable(inputs)
        if cuda:
            inputs = inputs.cuda()
        preds = torch.round(torch.sigmoid(model(inputs)))
        save_test_preds(test_dir, preds.cpu())

    print("Done")
    return model
