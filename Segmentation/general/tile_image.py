import math
import os
import random
import shutil
from glob import glob
from pathlib import Path

import numpy as np
import rasterio
import rasterio.mask
from PIL import Image
from rasterio.windows import Window
from torchvision.transforms import functional as transforms_f

tile_img_dir = Path("../../../data/roof_slope/images/")
tile_mask_dir = Path("../../../data/roof_slope/labels/")


def sliding_window(image, mask, stepSize, windowSize):
    # slide window across the image
    # incrementing their respective x  and y  counters by the provided step size.
    for y in range(0, image.shape[0], stepSize):
        for x in range(0, image.shape[1], stepSize):
            # yield the current window and x and y coordinates as tuple
            yield x, y, image[y:y + windowSize[1], x:x + windowSize[0]], mask[y:y + windowSize[1], x:x + windowSize[0]]


def split_into_train_test_val_dirs(tiles_src_path, train_val_split):
    tiles = glob(str(tiles_src_path / r"*.png"))
    # shuffle the images so there is a mix of terrains in train, val and test. Use 4 as the seed to get same
    # shuffle for masks and images
    random.Random(4).shuffle(tiles)
    # find 60%, 30% and 10% of that number
    num_train = math.floor(train_val_split[0] * len(tiles))
    num_val = math.floor(train_val_split[1] * len(tiles))
    train_images = tiles[0:num_train]
    val_images = tiles[num_train: num_train + num_val]
    test_images = tiles[num_train + num_val:]

    # move train, test and validation images into their respective directories
    for f_path in train_images:
        shutil.move(f_path, tiles_src_path / 'train' / Path(f_path).name)
    for f_path in val_images:
        shutil.move(f_path, tiles_src_path / 'val' / Path(f_path).name)
    for f_path in test_images:
        shutil.move(f_path, tiles_src_path / 'test' / Path(f_path).name)


def remove_files(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def split_image_to_tiles_for_testing(img_path, filetype, tiled_img_path, window_size, resize_size):
    # clear image's in test_images
    remove_files(tiled_img_path)

    full_size_images = glob(img_path + fr"\*{filetype}")
    for idx, image in enumerate(full_size_images):
        with rasterio.open(image) as im_src:
            for row_off in range(0, im_src.height, window_size):
                for col_off in range(0, im_src.width, window_size):
                    img_patch = im_src.read([1, 2, 3], window=Window(col_off, row_off, window_size, window_size))
                    # wrong shape because window doens't fit equally into width/height. So skip this image
                    if img_patch.shape != (3, window_size, window_size):
                        continue
                    # convert to int from float to ensure it can be previewed etc.
                    img_patch = Image.fromarray(img_patch.transpose(1, 2, 0).astype(np.uint8))
                    img_patch = transforms_f.resize(img_patch, resize_size)
                    img_patch.save(f"{tiled_img_path}/slope_seg_{idx}_{col_off}_{row_off}.png")


def split_image_to_tiles_for_training(img_path, mask_path, tiled_img_path, tiled_mask_path, window_size, resize_size,
                                      train_val_split):
    """iteratively divides the 3584x5120 image into 896x896 squares, then resize them to 512x512 for the segnet
       E.g. 20 images out of a 3584x5120 raster image (remainder is ignored) """

    # clear image's and masks' train, test and validation folders
    remove_files(tiled_img_path)
    remove_files(tiled_mask_path)
    os.mkdir(tiled_img_path / "train")
    os.mkdir(tiled_img_path / "test")
    os.mkdir(tiled_img_path / "val")
    os.mkdir(tiled_mask_path / "train")
    os.mkdir(tiled_mask_path / "test")
    os.mkdir(tiled_mask_path / "val")

    full_size_images = glob(str(img_path / r"*.tif"))
    full_size_labels = glob(str(mask_path / r"*.tif"))

    for idx, (image, label) in enumerate(zip(full_size_images, full_size_labels)):
        with rasterio.open(image) as im_src, rasterio.open(label) as label_src:
            # slide window over image ignoring a window-sized boundary around the edge to avoid polygons which were
            # truncated because they spilt over image boundary.
            for row_off in range(window_size, im_src.height - window_size, window_size):
                for col_off in range(window_size, im_src.width - window_size, window_size):
                    # skip patches where no mask currently exists (remove soon)
                    img_patch = im_src.read([1, 2, 3], window=Window(col_off, row_off, window_size, window_size))
                    msk_patch = label_src.read([1, 2, 3], window=Window(col_off, row_off, window_size, window_size))

                    # if > 20% of the input image is blank, skip this patch
                    if (np.count_nonzero(img_patch) / img_patch.size) < 0.80:
                        continue

                    # convert to int from float to ensure it can be previewed etc.
                    img_patch = Image.fromarray(img_patch.transpose(1, 2, 0).astype(np.uint8))
                    msk_patch = Image.fromarray(msk_patch.transpose(1, 2, 0).astype(np.uint8))
                    # plt.imshow(img_patch)

                    img_patch = transforms_f.resize(img_patch, resize_size)
                    # use nearest neighbour interpolation here so the mask boundaries don't blur as they would if
                    # bilinear interpolation is used.
                    msk_patch = transforms_f.resize(msk_patch, resize_size, interpolation=Image.NEAREST)

                    img_patch.save(tiled_img_path / f"slope_seg_{idx}_{col_off}_{row_off}.png")
                    msk_patch.save(tiled_mask_path / f"slope_seg_{idx}_{col_off}_{row_off}.png")

    split_into_train_test_val_dirs(tiled_img_path, train_val_split)
    split_into_train_test_val_dirs(tiled_mask_path, train_val_split)
