# README #
### What is this repository for? ###
The roof_slope_seg library holds all the deep learning and preprocessing code for the segmentation of individual roof slopes from satellite and aerial imager.

### What are the prerequisite? ###
To run the algorithm, a set of input images and their respective roof slope segmentation labels must be included.

### How do I get set up? ###

* To install all the packages required, create and activate a pip environment then run the command: 

"pip install -r requirements.txt"

* If you've added any new packages during development, please update the pip file using the command:

"pip freeze > requirements.txt ".

### Understanding the code ###
For information on the code structure and how it works, please contact me at joemo58@hotmail.co.uk.

### Contribution guidelines ###

* A consistent structure should be mainted when adding new tasks, including:
    * data should be stored in a seperate data directory that is ignored from the GIT
    * A shared 'general' AI library, which exposes the deep learninig functionalities with 
    enough configurability to be shared amongst the projects.
    * out and src directories for each individual project. 
    
* When updating the library, please take care to update the corresponding
 README files.
